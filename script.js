var isValid = function(s) {
    const map = {
        ")" : "(",
        "]" : "[",
        "}" : "{"        
    }
    let stack = [];
    for(let char of s){
        if(!map[char]){
            stack.push(char);
        } else {
            if(stack.lenth !== 0 && stack[stack.length - 1] === map[char]){
                stack.pop()
            }else{
                return false;
            }
        }
    }

    return stack.length === 0 ? true : false ;
};

const answer1 = isValid("()");
const answer2 = isValid("()[]{}");
const answer3 = isValid("(]");
const answer4 = isValid("{[]}");
const answer5 = isValid("{[][]}");
const answer6 = isValid("{[][)}");

console.log({
    answer1,
    answer2,
    answer3,
    answer4,
    answer5,
    answer6
})